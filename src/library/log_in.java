package library;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JPasswordField;
//import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.SwingConstants;

public class log_in extends JFrame {
	

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					log_in frame = new log_in();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public log_in() {
		setTitle("LogIn");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton_5 = new JButton("Admin Login");
		btnNewButton_5.setIcon(new ImageIcon(log_in.class.getResource("/image/admin-log in.png")));
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
					String sql="SELECT * FROM admin WHERE Username=? AND Password=?"; 
					
		        	PreparedStatement pst=conn.prepareStatement(sql);
		        	pst.setString(1,textField.getText());
		        	pst.setString(2,passwordField.getText());
		        	ResultSet rs=pst.executeQuery();
		        	
		        	if (rs.next()) {
		        		
		        		rs.close();
		        		pst.close();
		        		
		        		setVisible(false);
						Home ob=new Home();
						//ob.setUpLoading();
						ob.setVisible(true);
		        		
		        	}else {
		        		JOptionPane.showMessageDialog(null,"incorrect username and password");
		        	}
		   
		        	
		            
		           
		           
		           
		            
		            
		        } catch (Exception e1) {
		            JOptionPane.showMessageDialog(null,e1);
		            
		        }
				
				
				
			}
		});
		btnNewButton_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton_5.setBounds(795, 246, 148, 25);
		contentPane.add(btnNewButton_5);
		
		JLabel lblNewLabel = new JLabel("Welcome To Library");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Sitka Subheading", Font.BOLD, 65));
		lblNewLabel.setBounds(37, 42, 699, 90);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Student ID");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(653, 143, 118, 23);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(653, 188, 118, 23);
		contentPane.add(lblNewLabel_2);
		
		textField = new JTextField();
		textField.setBounds(781, 146, 148, 25);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("User Login");
		btnNewButton.setIcon(new ImageIcon(log_in.class.getResource("/image/user log in.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
	        	
				
//				String usernameLogin;
//				int passwordLogin;
//				
//				usernameLogin=textField.getText();
//				passwordLogin=Integer.parseInt(passwordField.getText());
				
				
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
					String sql="SELECT * FROM account WHERE Student_ID=? AND Password=?"; 
					
		        	PreparedStatement pst=conn.prepareStatement(sql);
		        	pst.setString(1,textField.getText());
		        	pst.setString(2,passwordField.getText());
		        	ResultSet rs=pst.executeQuery();
		        	
		        	if (rs.next()) {
		        		
		        		rs.close();
		        		pst.close();
		        		
		        		setVisible(false);
						UserHome ob=new UserHome(textField.getText() );
						//ob.setUpLoading();
						ob.setVisible(true);
		        		
		        	}else {
		        		JOptionPane.showMessageDialog(null,"incorrect username and password");
		        	}
		   
		        	
		            
		           
		           
		           
		            
		            
		        } catch (Exception e1) {
		            JOptionPane.showMessageDialog(null,e1);
		            
		        }
//				finally {
//					
//					try {
//						rs.close();
//		        		pst.close();
//						
//					}catch (Exception e) {
//						// TODO: handle exception
//					}
//				}
				
				
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.setBounds(653, 247, 132, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Forgot Password");
		btnNewButton_1.setIcon(new ImageIcon(log_in.class.getResource("/image/forgot-password.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				forgot_password ob=new forgot_password();
				ob.setVisible(true);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton_1.setBounds(709, 299, 178, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Sign Up");
		btnNewButton_2.setIcon(new ImageIcon(log_in.class.getResource("/image/sing up.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				signUp ob=new signUp();
				ob.setVisible(true);
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton_2.setBounds(739, 394, 112, 23);
		contentPane.add(btnNewButton_2);
		
		JLabel lblNewLabel_3 = new JLabel("Don't have an Account");
		lblNewLabel_3.setBounds(729, 355, 132, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setForeground(Color.BLACK);
		lblNewLabel_4.setIcon(new ImageIcon(log_in.class.getResource("/image/white.png")));
		lblNewLabel_4.setBackground(Color.WHITE);
		lblNewLabel_4.setBounds(633, 127, 319, 312);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5.setIcon(new ImageIcon(log_in.class.getResource("/image/login - Copy.png")));
		lblNewLabel_5.setBounds(0, 0, 984, 561);
		contentPane.add(lblNewLabel_5);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(781, 188, 148, 25);
		contentPane.add(passwordField);

		//setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{contentPane, lblNewLabel, lblNewLabel_1, lblNewLabel_2, passwordField, textField, btnNewButton, btnNewButton_1, btnNewButton_2, lblNewLabel_3, lblNewLabel_4, lblNewLabel_5}));
	}
}
