package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SwingConstants;




public class signUp extends JFrame {

	private JPanel contentPane;
	private JTextField name1;
	private JTextField pass;
	private JTextField answer1;
	private JTextField contactNum;
	private JTextField courses;
	private JTextField department;
	private JTextField years;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					signUp frame = new signUp();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public signUp() {
		setTitle("Sing Up");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 930, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_9_3 = new JLabel("Year");
		lblNewLabel_9_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_9_3.setBounds(491, 294, 127, 14);
		contentPane.add(lblNewLabel_9_3);
		
		years = new JTextField();
		years.setColumns(10);
		years.setBounds(628, 291, 185, 20);
		contentPane.add(years);
		
		JLabel lblNewLabel_9_2 = new JLabel("Department");
		lblNewLabel_9_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_9_2.setBounds(491, 232, 127, 14);
		contentPane.add(lblNewLabel_9_2);
		
		department = new JTextField();
		department.setColumns(10);
		department.setBounds(628, 229, 185, 20);
		contentPane.add(department);
		
		JLabel lblNewLabel_9_1 = new JLabel("Course");
		lblNewLabel_9_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_9_1.setBounds(491, 178, 127, 14);
		contentPane.add(lblNewLabel_9_1);
		
		courses = new JTextField();
		courses.setColumns(10);
		courses.setBounds(628, 177, 185, 20);
		contentPane.add(courses);
		
		contactNum = new JTextField();
		contactNum.setColumns(10);
		contactNum.setBounds(628, 123, 185, 20);
		contentPane.add(contactNum);
		
		JLabel lblNewLabel_9 = new JLabel("Contact Number");
		lblNewLabel_9.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_9.setBounds(491, 126, 127, 14);
		contentPane.add(lblNewLabel_9);
		
		JLabel lblNewLabel = new JLabel("Create an New Account");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(300, 49, 368, 34);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Name");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(113, 123, 78, 19);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel("Password");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(113, 176, 118, 19);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Security Question");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_4.setBounds(114, 232, 138, 19);
		contentPane.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Answer");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_5.setBounds(114, 290, 97, 19);
		contentPane.add(lblNewLabel_5);
		
		JComboBox comboBox1 = new JComboBox();
		comboBox1.setModel(new DefaultComboBoxModel(new String[] {"What is your mother Toungue?", "What is your nick name?", "What is your first childhood friend name?", "What is your school name?"}));
		comboBox1.setToolTipText("");
		comboBox1.setBounds(264, 230, 185, 22);
		contentPane.add(comboBox1);
		
		name1 = new JTextField();
		name1.setColumns(10);
		name1.setBounds(264, 123, 185, 20);
		contentPane.add(name1);
		
		pass = new JTextField();
		pass.setColumns(10);
		pass.setBounds(264, 177, 185, 20);
		contentPane.add(pass);
		
		JButton signup = new JButton("Register");
		signup.setIcon(new ImageIcon(signUp.class.getResource("/image/Register.jpg")));
		signup.setBackground(Color.WHITE);
		signup.setForeground(new Color(0, 0, 255));
		signup.setFont(new Font("Tahoma", Font.BOLD, 14));
		signup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				String name;
				String sec_Question;
				String answer;
				int password;
				String contact;
				String course;
				String depart;
				String year;
				
				
				
				name=name1.getText();
				sec_Question=(String) comboBox1.getSelectedItem();
				answer=answer1.getText();
				password=Integer.parseInt(pass.getText());
				contact=contactNum.getText();
				course=courses.getText();
				depart=department.getText();
				year=years.getText();
				
		        try {
		        	Class.forName("com.mysql.jdbc.Driver");
		        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
		        	String sql="INSERT INTO  account (Name,Password,Sec_Q,Answer,Contact_Number,Course,Department,Year) VALUES('"+name+"','"+password+"','"+sec_Question+"','"+answer+"','"+contact+"','"+course+"','"+depart+"','"+year+"')";
		        	PreparedStatement pst=conn.prepareStatement(sql);
		        	pst.execute();
		   
		            JOptionPane.showMessageDialog(null,"successfully entered data");
		            
		            setVisible(false);
					log_in ob=new log_in();
				
					ob.setVisible(true);
		            

		           
		           
		           
		            
		            
		        } catch (Exception e1) {
		            JOptionPane.showMessageDialog(null,e1);
		            
		        }
				
				
			}
		});
		signup.setBounds(346, 362, 127, 41);
		contentPane.add(signup);
		
		answer1 = new JTextField();
		answer1.setColumns(10);
		answer1.setBounds(264, 293, 185, 20);
		contentPane.add(answer1);
		
		
		JButton btnNewButton_1 = new JButton("Back");
		btnNewButton_1.setIcon(new ImageIcon(signUp.class.getResource("/image/back-icon.png")));
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_1.setForeground(new Color(0, 0, 255));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				log_in ob=new log_in();
				ob.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(522, 362, 112, 41);
		contentPane.add(btnNewButton_1);
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setBackground(new Color(0, 250, 154));
		lblNewLabel_6.setIcon(new ImageIcon(signUp.class.getResource("/image/white.png")));
		lblNewLabel_6.setBounds(71, 35, 817, 431);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon(signUp.class.getResource("/image/login - Copy.png")));
		lblNewLabel_7.setBounds(0, 0, 984, 561);
		contentPane.add(lblNewLabel_7);
		
		JLabel label = new JLabel("New label");
		label.setBounds(476, 126, 46, 14);
		contentPane.add(label);
		
		JLabel lblNewLabel_8 = new JLabel("New label");
		lblNewLabel_8.setBounds(507, 126, 46, 14);
		contentPane.add(lblNewLabel_8);
	}
}
