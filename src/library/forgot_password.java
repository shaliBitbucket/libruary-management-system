package library;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class forgot_password extends JFrame {


	
	
	
	public void search() {
		String a1=textField.getText(); 
		String sql="select * From account Where Student_ID='"+a1+"'";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
		
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
		if (rs.next()) {
			textField_1.setText(rs.getString(2));
			textField_2.setText(rs.getString(4));
			rs.close();
			pst.close();
		}else {
			JOptionPane.showMessageDialog(null, "Incorrect User Name");
		}
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
	}
	
	public void Retrive() {
	String a1=textField.getText();
	String a2=textField_3.getText();
	String sql="select * From account Where Student_ID='"+a1+"'" ;
	try {
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
		
		PreparedStatement pst=conn.prepareStatement(sql);
		ResultSet rs=pst.executeQuery();
		if (rs.next()) {
			
			if(a2.equals(rs.getString(5)))
			{
				textField_4.setText(rs.getString(3));
				
			}else {
				
				JOptionPane.showMessageDialog(null,"Incorrect Answer" );
			}			
			
			
			
		}
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}	
	}
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JButton btnNewButton;
	private JButton btnNewButton_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					forgot_password frame = new forgot_password();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public forgot_password() {
		setTitle("Fogot Pssword");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Get Forgot Password");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(391, 74, 216, 25);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("User Name");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(324, 151, 91, 20);
		contentPane.add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(495, 153, 117, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Name");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(324, 208, 68, 17);
		contentPane.add(lblNewLabel_2);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(495, 208, 117, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Answer");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_3.setBounds(324, 322, 68, 17);
		contentPane.add(lblNewLabel_3);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(495, 266, 117, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Your Security Question");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_4.setBounds(324, 265, 161, 19);
		contentPane.add(lblNewLabel_4);
		
		textField_3 = new JTextField();
		textField_3.setBounds(495, 322, 117, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Your Password is");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_5.setBounds(424, 439, 134, 14);
		contentPane.add(lblNewLabel_5);
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(424, 464, 117, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		btnNewButton = new JButton("Retrive");
		btnNewButton.setIcon(new ImageIcon(forgot_password.class.getResource("/image/retrive.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Retrive();
			}
		});
		btnNewButton.setBounds(427, 375, 108, 23);
		contentPane.add(btnNewButton);
		
		btnNewButton_1 = new JButton("Search");
		btnNewButton_1.setIcon(new ImageIcon(forgot_password.class.getResource("/image/search.png")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				search();
			}
		});
		btnNewButton_1.setBounds(622, 152, 102, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Back");
		btnNewButton_2.setIcon(new ImageIcon(forgot_password.class.getResource("/image/back-icon.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				log_in ob=new log_in();
				ob.setVisible(true);
			}
		});
		btnNewButton_2.setBounds(433, 513, 102, 23);
		contentPane.add(btnNewButton_2);
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setIcon(new ImageIcon(forgot_password.class.getResource("/image/white.png")));
		lblNewLabel_6.setBounds(285, 57, 451, 493);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon(forgot_password.class.getResource("/image/login - Copy.png")));
		lblNewLabel_7.setBounds(0, 0, 984, 561);
		contentPane.add(lblNewLabel_7);
	}
}
