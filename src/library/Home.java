package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import java.awt.Window.Type;

public class Home extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home frame = new Home();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Home() {
		setTitle("HOME");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 819, 663);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Exit");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mntmNewMenuItem.setIcon(new ImageIcon(Home.class.getResource("/image/exit.png")));
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Logout");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				log_in ob =new log_in();
				ob.setVisible(true);
			}
		});
		mntmNewMenuItem_1.setIcon(new ImageIcon(Home.class.getResource("/image/logout.jpg")));
		mnNewMenu.add(mntmNewMenuItem_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setBounds(10, 11, 240, 137);
		lblLogo.setIcon(new ImageIcon(Home.class.getResource("/image/logo.png")));
		
		JLabel lblNewLabel_1 = new JLabel("Libary Management System");
		lblNewLabel_1.setBounds(535, 73, 213, 27);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel lblNewLabel = new JLabel("Wellcome");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(438, 11, 341, 70);
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Century Gothic", Font.BOLD, 56));
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setBounds(56, 184, 150, 130);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				NewBook ob =new NewBook();
				ob.setVisible(true);
			}
		});
		btnNewButton.setIcon(new ImageIcon(Home.class.getResource("/image/New_book.png")));
		
		JLabel lblNewLabel_2 = new JLabel("New Book");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(78, 313, 111, 34);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Statistic ob =new Statistic();
				ob.setVisible(true);
				
			}
		});
		btnNewButton_1.setBounds(328, 184, 150, 130);
		btnNewButton_1.setIcon(new ImageIcon(Home.class.getResource("/image/statistic.png")));
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setBounds(600, 184, 150, 130);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				New_student ob =new New_student();
				ob.setVisible(true);
			}
		});
		btnNewButton_2.setIcon(new ImageIcon(Home.class.getResource("/image/Add_member.png")));
		contentPane.setLayout(null);
		contentPane.add(lblLogo);
		contentPane.add(lblNewLabel_1);
		contentPane.add(lblNewLabel);
		contentPane.add(btnNewButton);
		contentPane.add(lblNewLabel_2);
		contentPane.add(btnNewButton_1);
		contentPane.add(btnNewButton_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("Statistic");
		lblNewLabel_2_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_1.setForeground(Color.BLUE);
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2_1.setBounds(328, 320, 150, 27);
		contentPane.add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_2_2 = new JLabel(" Student Details");
		lblNewLabel_2_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_2.setForeground(Color.BLUE);
		lblNewLabel_2_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2_2.setBounds(600, 325, 150, 22);
		contentPane.add(lblNewLabel_2_2);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Issue_Book ob =new Issue_Book();
				ob.setVisible(true);
			}
		});
		btnNewButton_3.setIcon(new ImageIcon(Home.class.getResource("/image/Issue book.png")));
		btnNewButton_3.setBounds(56, 417, 150, 130);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Return_Book ob =new Return_Book ();
				ob.setVisible(true);
			}
		});
		btnNewButton_4.setIcon(new ImageIcon(Home.class.getResource("/image/images.png")));
		btnNewButton_4.setBounds(328, 417, 150, 130);
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(Home.class.getResource("/image/about.png")));
		btnNewButton_5.setBounds(600, 417, 150, 130);
		contentPane.add(btnNewButton_5);
		
		JLabel lblNewLabel_2_3 = new JLabel("Issue Book");
		lblNewLabel_2_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2_3.setForeground(Color.BLUE);
		lblNewLabel_2_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_3.setBounds(56, 558, 150, 22);
		contentPane.add(lblNewLabel_2_3);
		
		JLabel lblNewLabel_2_1_1 = new JLabel("Return Book");
		lblNewLabel_2_1_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_1_1.setForeground(Color.BLUE);
		lblNewLabel_2_1_1.setBounds(328, 558, 150, 22);
		contentPane.add(lblNewLabel_2_1_1);
		
		JLabel lblNewLabel_2_1_1_1 = new JLabel("About");
		lblNewLabel_2_1_1_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2_1_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2_1_1_1.setForeground(Color.BLUE);
		lblNewLabel_2_1_1_1.setBounds(600, 558, 150, 22);
		contentPane.add(lblNewLabel_2_1_1_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 255, 127));
		panel.setBorder(new TitledBorder(null, "Operation", TitledBorder.LEADING, TitledBorder.TOP, null, Color.RED));
		panel.setBounds(20, 159, 759, 198);
		contentPane.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(176, 196, 222));
		panel_1.setBorder(new TitledBorder(null, "Action", TitledBorder.LEADING, TitledBorder.TOP, null, Color.RED));
		panel_1.setBounds(24, 379, 755, 212);
		contentPane.add(panel_1);
	}

}
