package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.JTable;
import javax.swing.JFormattedTextField;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.SystemColor;
import javax.swing.ImageIcon;
import java.text.SimpleDateFormat;  
import java.util.Date;  

import com.toedter.calendar.IDateEditor;
import com.toedter.calendar.JDateChooser;


public class Issue_Book extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Issue_Book frame = new Issue_Book();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Issue_Book() {
		setTitle("Issue Book");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 805, 468);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBookID = new JLabel("Book ID");
		lblBookID.setForeground(Color.BLACK);
		lblBookID.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblBookID.setBounds(30, 75, 68, 17);
		contentPane.add(lblBookID);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(120, 71, 105, 20);
		contentPane.add(textField);
		
		JLabel lblBookName = new JLabel("Book Name");
		lblBookName.setForeground(Color.BLACK);
		lblBookName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblBookName.setBounds(30, 114, 68, 17);
		contentPane.add(lblBookName);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(120, 111, 105, 20);
		contentPane.add(textField_1);
		
		JLabel lblEdition = new JLabel("Edition");
		lblEdition.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEdition.setBounds(30, 159, 68, 17);
		contentPane.add(lblEdition);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(120, 155, 105, 20);
		contentPane.add(textField_2);
		
		JLabel lblPublisher = new JLabel("Publisher");
		lblPublisher.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPublisher.setBounds(30, 198, 68, 17);
		contentPane.add(lblPublisher);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(120, 194, 105, 20);
		contentPane.add(textField_3);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPrice.setBounds(30, 235, 68, 17);
		contentPane.add(lblPrice);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(120, 231, 105, 20);
		contentPane.add(textField_4);
		
		JLabel lblPages = new JLabel("Pages");
		lblPages.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPages.setBounds(30, 271, 68, 17);
		contentPane.add(lblPages);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(120, 268, 105, 20);
		contentPane.add(textField_5);
		
		JButton btnNewButton = new JButton("Search");
		btnNewButton.setIcon(new ImageIcon(Issue_Book.class.getResource("/image/search.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String a1=textField.getText(); 
				
				try {
					String sql="select * From book Where Book_ID='"+a1+"'";
					Class.forName("com.mysql.jdbc.Driver");
					Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
				
					PreparedStatement pst=conn.prepareStatement(sql);
					ResultSet rs=pst.executeQuery();
				if (rs.next()) {
					textField_1.setText(rs.getString(2));
					textField_2.setText(rs.getString(3));
					textField_3.setText(rs.getString(4));
					textField_4.setText(rs.getString(5));
					textField_5.setText(rs.getString(6));
					
					rs.close();
					pst.close();
				}else {
					JOptionPane.showMessageDialog(null, "Incorrect Book ID");
				}
				}catch(Exception e1) {
					JOptionPane.showMessageDialog(null, e1);
				}
				
				
				
			}
		});
		btnNewButton.setBounds(236, 73, 105, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setForeground(Color.BLACK);
		lblYear.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblYear.setBounds(377, 271, 105, 14);
		contentPane.add(lblYear);
		
		JLabel lblCourse = new JLabel("Course");
		lblCourse.setForeground(Color.BLACK);
		lblCourse.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCourse.setBounds(377, 194, 105, 14);
		contentPane.add(lblCourse);
		
		JLabel lblDepartmaent = new JLabel("Departmaent");
		lblDepartmaent.setForeground(Color.BLACK);
		lblDepartmaent.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDepartmaent.setBounds(377, 231, 105, 14);
		contentPane.add(lblDepartmaent);
		
		JLabel lblContactNo = new JLabel("Contact No");
		lblContactNo.setForeground(Color.BLACK);
		lblContactNo.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblContactNo.setBounds(377, 155, 105, 14);
		contentPane.add(lblContactNo);
		
		JLabel lblName = new JLabel("Name");
		lblName.setForeground(Color.BLACK);
		lblName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblName.setBounds(377, 115, 105, 14);
		contentPane.add(lblName);
		
		JLabel lblStutudentID = new JLabel("Stutudent ID");
		lblStutudentID.setForeground(Color.BLACK);
		lblStutudentID.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblStutudentID.setBounds(377, 78, 105, 14);
		contentPane.add(lblStutudentID);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(492, 75, 152, 20);
		contentPane.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(492, 112, 152, 20);
		contentPane.add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(492, 152, 152, 20);
		contentPane.add(textField_8);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(492, 192, 152, 20);
		contentPane.add(textField_9);
		
		JButton btnSerch = new JButton("Search");
		btnSerch.setIcon(new ImageIcon(Issue_Book.class.getResource("/image/search.png")));
		btnSerch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
					String a2=textField_6.getText(); 
				
				try {
					String sql="select * From account Where Student_ID='"+a2+"'";
					Class.forName("com.mysql.jdbc.Driver");
					Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
				
					PreparedStatement pst=conn.prepareStatement(sql);
					ResultSet rs=pst.executeQuery();
				if (rs.next()) {
					textField_7.setText(rs.getString(2));
					textField_8.setText(rs.getString(6));
					textField_9.setText(rs.getString(7));
					textField_10.setText(rs.getString(8));
					textField_11.setText(rs.getString(9));
					
					rs.close();
					pst.close();
				}else {
					JOptionPane.showMessageDialog(null, "Incorrect Student ID");
				}
				}catch(Exception e1) {
					JOptionPane.showMessageDialog(null, e1);
				}
			}
		});
		btnSerch.setBounds(654, 73, 105, 23);
		contentPane.add(btnSerch);
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		textField_10.setBounds(492, 234, 152, 20);
		contentPane.add(textField_10);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(492, 270, 152, 20);
		contentPane.add(textField_11);
		
		JLabel lblNewLabel = new JLabel("Date of issue");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(279, 349, 146, 17);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton_1 = new JButton("Issue Book");
		btnNewButton_1.setIcon(new ImageIcon(Issue_Book.class.getResource("/image/issue.png")));

		btnNewButton_1.addActionListener(new ActionListener() {
			private JLabel dateChooser;

			public void actionPerformed(ActionEvent e) {
				
				String b_id=textField.getText();
				String b_name=textField_1.getText();
				String b_edition=textField_2.getText();
				String b_publisher=textField_3.getText();
				String b_price=textField_4.getText();
				String b_pages=textField_5.getText();
				String s_id=textField_6.getText();
				String s_name=textField_7.getText();
				String s_contact=textField_8.getText();
				String s_course=textField_9.getText();
				String s_department=textField_10.getText();
				String s_year=textField_11.getText();
				String s_DOI=textField_12.getText();
				
				
				
			
				
				
				 try {
			        	Class.forName("com.mysql.jdbc.Driver");
			        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
			        	String sql="INSERT INTO  issue (Book_ID,Book_Name,Edition,Publisher,Price,Pages,Student_ID,Name,Contact_Number,Course,Department,Year,DOI) VALUES('"+b_id+"','"+ b_name+"','"+b_edition+"','"+b_publisher+"','"+b_price+"','"+b_pages+"','"+s_id+"','"+s_name+"','"+s_contact+"','"+s_course+"','"+ s_department+"','"+s_year+"','"+s_DOI+"')";
			        	PreparedStatement pst=conn.prepareStatement(sql);
			        	pst.execute();
			   
			            JOptionPane.showMessageDialog(null,"successfully entered data");
			            
			            
			            
			           
			           
			           
			            
			            
			        } catch (Exception e1) {
			            JOptionPane.showMessageDialog(null,e1);
			            
			        }
				
				
			}
		});
		btnNewButton_1.setBounds(342, 378, 124, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Back");
		btnNewButton_2.setIcon(new ImageIcon(Issue_Book.class.getResource("/image/back-icon.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Home ob = new Home();
				ob.setVisible(true);
			}
		});
		btnNewButton_2.setBounds(505, 378, 105, 23);
		contentPane.add(btnNewButton_2);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Book Detaile", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 255)));
		panel.setBounds(10, 44, 337, 282);
		contentPane.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Student Detaie", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel_1.setBounds(355, 44, 410, 282);
		contentPane.add(panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(SystemColor.inactiveCaption);
		panel_2.setForeground(SystemColor.activeCaption);
		panel_2.setBounds(279, 337, 365, 81);
		contentPane.add(panel_2);
		
		textField_12 = new JTextField();
		panel_2.add(textField_12);
		textField_12.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Issue_Book.class.getResource("/image/login - Copy.png")));
		lblNewLabel_1.setBounds(0, 0, 789, 429);
		contentPane.add(lblNewLabel_1);
	}
}
