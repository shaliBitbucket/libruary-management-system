package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import com.toedter.calendar.JDateChooser;

public class Return_Book extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JLabel lblStutudentID;
	private JTextField textField_6;
	private JTextField textField_7;
	private JLabel lblContactNo;
	private JTextField textField_8;
	private JLabel lblCourse;
	private JTextField textField_9;
	private JLabel lblDepartmaent;
	private JTextField textField_10;
	private JLabel lblYear;
	private JTextField textField_11;
	private JLabel lblName;
	private JLabel lblDateOfIssue;
	private JTextField textField_12;
	private JPanel panel;
	private JLabel lblDateOfRetern;
	private JButton btnRetern;
	private JButton btnNewButton_2;
	private JTextField textField_13;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Return_Book frame = new Return_Book();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Return_Book() {
		setTitle("Return Book");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 747, 479);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(139, 252, 105, 20);
		contentPane.add(textField);
		
		JLabel lblPages = new JLabel("Pages");
		lblPages.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPages.setBounds(31, 254, 68, 17);
		contentPane.add(lblPages);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPrice.setBounds(31, 218, 68, 17);
		contentPane.add(lblPrice);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(139, 215, 105, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(139, 178, 105, 20);
		contentPane.add(textField_2);
		
		JLabel lblPublisher = new JLabel("Publisher");
		lblPublisher.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPublisher.setBounds(31, 181, 68, 17);
		contentPane.add(lblPublisher);
		
		JLabel lblEdition = new JLabel("Edition");
		lblEdition.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEdition.setBounds(31, 142, 68, 17);
		contentPane.add(lblEdition);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(139, 139, 105, 20);
		contentPane.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(139, 95, 105, 20);
		contentPane.add(textField_4);
		
		JLabel lblBookName = new JLabel("Book Name");
		lblBookName.setForeground(Color.BLACK);
		lblBookName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblBookName.setBounds(31, 97, 68, 17);
		contentPane.add(lblBookName);
		
		JLabel lblBookID = new JLabel("Book ID");
		lblBookID.setForeground(Color.BLACK);
		lblBookID.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblBookID.setBounds(31, 58, 68, 17);
		contentPane.add(lblBookID);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(139, 55, 105, 20);
		contentPane.add(textField_5);
		
		JButton btnNewButton = new JButton("Search");
		btnNewButton.setIcon(new ImageIcon(Return_Book.class.getResource("/image/search.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String a1=textField_5.getText(); 
				
				try {
					String sql="select * From issue Where Book_ID='"+a1+"'";
					Class.forName("com.mysql.jdbc.Driver");
					Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
				
					PreparedStatement pst=conn.prepareStatement(sql);
					ResultSet rs=pst.executeQuery();
				if (rs.next()) {
					textField_4.setText(rs.getString(2));
					textField_3.setText(rs.getString(3));
					textField_2.setText(rs.getString(4));
					textField_1.setText(rs.getString(5));
					textField.setText(rs.getString(6));
					textField_6.setText(rs.getString(7));
					textField_7.setText(rs.getString(8));
					textField_8.setText(rs.getString(9));
					textField_9.setText(rs.getString(10));
					textField_10.setText(rs.getString(11));
					textField_11.setText(rs.getString(12));
					textField_12.setText(rs.getString(13));
					
					
					
					rs.close();
					pst.close();
				}else {
					JOptionPane.showMessageDialog(null, "Incorrect Book ID");
				}
				}catch(Exception e1) {
					JOptionPane.showMessageDialog(null, e1);
				}
				
			}
		});
		btnNewButton.setBounds(254, 56, 105, 23);
		contentPane.add(btnNewButton);
		
		lblStutudentID = new JLabel("Stutudent ID");
		lblStutudentID.setForeground(Color.BLACK);
		lblStutudentID.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblStutudentID.setBounds(377, 61, 105, 14);
		contentPane.add(lblStutudentID);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(492, 58, 152, 20);
		contentPane.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(492, 95, 152, 20);
		contentPane.add(textField_7);
		
		lblContactNo = new JLabel("Contact No");
		lblContactNo.setForeground(Color.BLACK);
		lblContactNo.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblContactNo.setBounds(377, 138, 105, 14);
		contentPane.add(lblContactNo);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(492, 135, 152, 20);
		contentPane.add(textField_8);
		
		lblCourse = new JLabel("Course");
		lblCourse.setForeground(Color.BLACK);
		lblCourse.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCourse.setBounds(377, 177, 105, 14);
		contentPane.add(lblCourse);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(492, 175, 152, 20);
		contentPane.add(textField_9);
		
		lblDepartmaent = new JLabel("Departmaent");
		lblDepartmaent.setForeground(Color.BLACK);
		lblDepartmaent.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDepartmaent.setBounds(377, 214, 105, 14);
		contentPane.add(lblDepartmaent);
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		textField_10.setBounds(492, 217, 152, 20);
		contentPane.add(textField_10);
		
		lblYear = new JLabel("Year");
		lblYear.setForeground(Color.BLACK);
		lblYear.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblYear.setBounds(377, 254, 105, 14);
		contentPane.add(lblYear);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(492, 253, 152, 20);
		contentPane.add(textField_11);
		
		lblName = new JLabel("Name");
		lblName.setForeground(Color.BLACK);
		lblName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblName.setBounds(377, 98, 105, 14);
		contentPane.add(lblName);
		
		lblDateOfIssue = new JLabel("Date of issue");
		lblDateOfIssue.setForeground(Color.BLACK);
		lblDateOfIssue.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDateOfIssue.setBounds(325, 299, 105, 14);
		contentPane.add(lblDateOfIssue);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(440, 297, 152, 20);
		contentPane.add(textField_12);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Reten Panal", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel.setBounds(10, 26, 680, 309);
		contentPane.add(panel);
		
		lblDateOfRetern = new JLabel("Date of Return");
		lblDateOfRetern.setForeground(Color.BLACK);
		lblDateOfRetern.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDateOfRetern.setBounds(327, 360, 105, 14);
		contentPane.add(lblDateOfRetern);
		
		btnRetern = new JButton("Return");
		btnRetern.setIcon(new ImageIcon(Return_Book.class.getResource("/image/return.png")));
		btnRetern.addActionListener(new ActionListener() {
			private JTextComponent dateChooser;

			public void actionPerformed(ActionEvent e) {
				
				String b_id=textField_5.getText();
				String b_name=textField_4.getText();
				String b_edition=textField_3.getText();
				String b_publisher=textField_2.getText();
				String b_price=textField_1.getText();
				String b_pages=textField.getText();
				String s_id=textField_6.getText();
				String s_name=textField_7.getText();
				String s_contact=textField_8.getText();
				String s_course=textField_9.getText();
				String s_department=textField_10.getText();
				String s_year=textField_11.getText();
				String s_DOI=textField_12.getText();
				String s_DOR=textField_13.getText();
				
				
				 try {
			        	Class.forName("com.mysql.jdbc.Driver");
			        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
			        	String sql="INSERT INTO  returnbooks (Book_ID,BName,Edition,Publisher,Price,Pages,Student_ID,Name,Contact_Number,Course,Department,Year,DOI,DOR) VALUES('"+b_id+"','"+ b_name+"','"+b_edition+"','"+b_publisher+"','"+b_price+"','"+b_pages+"','"+s_id+"','"+s_name+"','"+s_contact+"','"+s_course+"','"+ s_department+"','"+s_year+"','"+s_DOI+"','"+s_DOR+"')";
			        	PreparedStatement pst=conn.prepareStatement(sql);
			        	pst.execute();
			        	
			        	
			        	String sql1="DELETE FROM issue WHERE Book_ID='"+b_id+"'";
			        	PreparedStatement pst1=conn.prepareStatement(sql1);
			        	pst1.execute();
			   
			            JOptionPane.showMessageDialog(null,"successfully entered data");
			            
			            
			            
			           
			           
			           
			            
			            
			        } catch (Exception e1) {
			            JOptionPane.showMessageDialog(null,e1);
			            
			        }
				
			}
		});
		btnRetern.setBounds(377, 393, 100, 23);
		contentPane.add(btnRetern);
		
		btnNewButton_2 = new JButton("Back");
		btnNewButton_2.setIcon(new ImageIcon(Return_Book.class.getResource("/image/back-icon.png")));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Home ob = new Home();
				ob.setVisible(true);
			}
		});
		btnNewButton_2.setBounds(506, 393, 100, 23);
		contentPane.add(btnNewButton_2);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.inactiveCaption);
		panel_1.setBounds(304, 346, 351, 83);
		contentPane.add(panel_1);
		
		textField_13 = new JTextField();
		panel_1.add(textField_13);
		textField_13.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Return_Book.class.getResource("/image/login - Copy.png")));
		lblNewLabel.setBounds(0, 1, 731, 439);
		contentPane.add(lblNewLabel);
	}
}
