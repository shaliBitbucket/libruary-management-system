package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.awt.event.ActionEvent;
import java.awt.Window.Type;
import java.awt.SystemColor;

public class NewBook extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewBook frame = new NewBook();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewBook() {
		setTitle("Add Book");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 692, 484);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPages = new JLabel("Pages");
		lblPages.setForeground(SystemColor.desktop);
		lblPages.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPages.setBounds(157, 225, 86, 17);
		contentPane.add(lblPages);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setForeground(SystemColor.desktop);
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPrice.setBounds(157, 189, 86, 17);
		contentPane.add(lblPrice);
		
		JLabel lblPublisher = new JLabel("Publisher");
		lblPublisher.setForeground(SystemColor.desktop);
		lblPublisher.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPublisher.setBounds(157, 152, 86, 17);
		contentPane.add(lblPublisher);
		
		JLabel lblEdition = new JLabel("Edition");
		lblEdition.setForeground(SystemColor.desktop);
		lblEdition.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEdition.setBounds(157, 113, 86, 17);
		contentPane.add(lblEdition);
		
		JLabel lblBookName = new JLabel("Book Name");
		lblBookName.setForeground(SystemColor.desktop);
		lblBookName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblBookName.setBounds(157, 68, 86, 17);
		contentPane.add(lblBookName);
		
		JLabel lblBookID = new JLabel("Book ID");
		lblBookID.setForeground(SystemColor.desktop);
		lblBookID.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblBookID.setBounds(157, 29, 86, 17);
		contentPane.add(lblBookID);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(313, 28, 248, 20);
		contentPane.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(313, 67, 248, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(313, 112, 248, 20);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(313, 151, 248, 20);
		contentPane.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(313, 188, 248, 20);
		contentPane.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(313, 224, 248, 20);
		contentPane.add(textField_5);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String b_id=textField.getText();
				String b_name=textField_1.getText();
				String b_edition=textField_2.getText();
				String b_publisher=textField_3.getText();
				String b_price=textField_4.getText();
				String b_pages=textField_5.getText();
				
				
				
				 try {
			        	Class.forName("com.mysql.jdbc.Driver");
			        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
			        	String sql="INSERT INTO  book (Book_ID,Name,Edition,Publisher,Price,Pages) VALUES('"+b_id+"','"+ b_name+"','"+b_edition+"','"+b_publisher+"','"+b_price+"','"+b_pages+"')";
			        	PreparedStatement pst=conn.prepareStatement(sql);
			        	pst.execute();
			   
			            JOptionPane.showMessageDialog(null,"successfully entered data");
			            
			            
			            
			           
			           
			           
			            
			            
			        } catch (Exception e1) {
			            JOptionPane.showMessageDialog(null,e1);
			            
			        }
				
				
				
				
			}
		});
		btnAdd.setIcon(new ImageIcon(NewBook.class.getResource("/image/add-icon.png")));
		btnAdd.setBounds(313, 296, 113, 41);
		contentPane.add(btnAdd);
		
		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Home ob=new Home();
				ob.setVisible(true);
			}
		});
		btnBack.setIcon(new ImageIcon(NewBook.class.getResource("/image/back-icon.png")));
		btnBack.setBounds(454, 296, 107, 41);
		contentPane.add(btnBack);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.inactiveCaptionBorder);
		panel.setBounds(133, 11, 463, 348);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(NewBook.class.getResource("/image/login - Copy.png")));
		lblNewLabel.setBounds(0, 0, 676, 445);
		contentPane.add(lblNewLabel);
	}

}
