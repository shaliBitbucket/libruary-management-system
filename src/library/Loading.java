package library;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;


import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import java.sql.Connection;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Point;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class Loading extends JFrame implements Runnable {
	Connection conn;
	int s=0;
	Thread th;
	
	private JFrame frame;
	private JProgressBar progressBar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Loading window = new Loading();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Loading() {	
		super("Loading");
		initialize();
		th = new Thread ((Runnable)this);
	}
	
	public void setUpLoading() {
		setVisible(false);
		th.start();
	}
	


	public void Run() {
		try {
			for(int i=1; i<=200;i++) {
				s=s+1;
				int m =progressBar.getMaximum();
				int v =progressBar.getValue();
				if (v<m) {
					progressBar.setValue(progressBar.getValue()+1);
				}
				else {
					i=201;
					setVisible(false);
					Home ob= new Home();
					ob.setVisible(true);
				}
				Thread.sleep(50);
				
			}
			
		}
		catch(Exception e){
			JOptionPane.showMessageDialog(null, e);
		}
	}



	/**
	 * Initialize the contents of the frame.
	 */
private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 342, 382);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Smart Libary");
		lblNewLabel.setBounds(27, 30, 264, 45);
		lblNewLabel.setForeground(new Color(51, 204, 255));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 27));
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(27, 106, 264, 14);
		progressBar.setStringPainted(true);
		
		
		JLabel lblNewLabel_1 = new JLabel("Please wait...");
		lblNewLabel_1.setBounds(129, 131, 76, 14);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(53, 169, 211, 139);
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\waruna\\Desktop\\New folder (4)\\libruary-management-system\\bin\\image\\loading.gif"));
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 306, 321);
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(lblNewLabel);
		frame.getContentPane().add(progressBar);
		frame.getContentPane().add(lblNewLabel_1);
		frame.getContentPane().add(lblNewLabel_2);
		frame.getContentPane().add(panel);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}


}
