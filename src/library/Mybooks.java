package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Mybooks extends JFrame {

	private JPanel contentPane;
	private JTable table;
	DefaultTableModel model;
	private String userid;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mybooks frame = new Mybooks();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Mybooks() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 724, 417);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("My Books");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBackground(SystemColor.inactiveCaptionBorder);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(289, 11, 144, 27);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 49, 688, 303);
		contentPane.add(scrollPane);
		
		table = new JTable();
		model=new DefaultTableModel();
		Object[] columns= {"Book_ID","Book_Name","Edition","Publisher","Price","Pages"};
		Object[] row=new Object[0];
		model.setColumnIdentifiers(columns);
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UserHome ob =new UserHome();
				ob.setVisible(true);
			}
		});
		btnNewButton.setIcon(new ImageIcon(Mybooks.class.getResource("/image/back-icon.png")));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.setBounds(10, 11, 108, 27);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(Mybooks.class.getResource("/image/login - Copy.png")));
		lblNewLabel_1.setBounds(0, 0, 708, 378);
		contentPane.add(lblNewLabel_1);
		
		
	}

	/**
	 * Create the frame.
	 */
	public Mybooks(String userid) {
		
//		Mybooks frame = new Mybooks();
//		frame.setVisible(true);
//		frame.setLocationRelativeTo(null);
		
		this.userid=userid;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("My Books");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel.setBounds(155, 11, 144, 27);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 49, 414, 186);
		contentPane.add(scrollPane);
		
		table = new JTable();
		model=new DefaultTableModel();
		Object[] columns= {"Book_ID","Book_Name","Edition","Publisher","Price","Pages"};
		Object[] row=new Object[0];
		model.setColumnIdentifiers(columns);
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				UserHome ob =new UserHome();
				ob.setVisible(true);
			}
		});
		btnNewButton.setIcon(new ImageIcon(Mybooks.class.getResource("/image/back-icon.png")));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.setBounds(10, 11, 108, 27);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(Mybooks.class.getResource("/image/login - Copy.png")));
		lblNewLabel_1.setBounds(0, 0, 708, 378);
		contentPane.add(lblNewLabel_1);
		
		dataLoad();
		
		
	}
	public void dataLoad() {
		
		
		try {
			
			String sql="select Book_ID,Book_Name,Edition,Publisher,Price From issue Where Student_ID='"+userid+"'";
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
		
			PreparedStatement pst=conn.prepareStatement(sql);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
			
	} catch (Exception e) {
		JOptionPane.showMessageDialog(null, e);
	}
	
		
		
		
	}
}
