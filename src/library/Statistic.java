package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import java.awt.Color;
import javax.swing.JProgressBar;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import javax.swing.border.EtchedBorder;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Statistic extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table_1;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	DefaultTableModel model,model1;
	private JPanel panel;
	private JPanel panel_1;
	private JLabel lblNewLabel;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Statistic frame = new Statistic();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Statistic() {
		setTitle("Statistic");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 789, 588);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(32, 34, 699, 164);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setBorder(null);
		table.setFont(new Font("Tahoma", Font.PLAIN, 11));
		table.setBackground(Color.WHITE);
		model=new DefaultTableModel();
		Object[] columns= {"Book_ID","Book_Name","Edition","Stdent ID","Student Name"};
		Object[] row=new Object[0];
		model.setColumnIdentifiers(columns);
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(32, 292, 699, 172);
		contentPane.add(scrollPane_1);
		//Object[] columns= {"Book_ID","Book_Name","Edition","Publisher","Price","Pages"};
		
		table_1 = new JTable();
		scrollPane_1.setViewportView(table_1);
		table_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		table_1.setBorder(null);
		table_1.setBackground(Color.WHITE);
//		model1=new DefaultTableModel();
//		Object[] columns1= {"Student_ID","Name","Contact Number","Course","Department","Year"};
//		Object[] row1=new Object[0];
//		model1.setColumnIdentifiers(columns1);
//		table_1.setModel(model1);
//		scrollPane.setViewportView(table_1);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "ISSUE BOKKS", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 742, 210);
		contentPane.add(panel);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "RETURN BOOKS", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 267, 742, 220);
		contentPane.add(panel_1);
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.setIcon(new ImageIcon(Statistic.class.getResource("/image/back-icon.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false);
				Home ob=new Home();
				ob.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnNewButton.setBounds(344, 497, 103, 41);
		contentPane.add(btnNewButton);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Statistic.class.getResource("/image/login - Copy.png")));
		lblNewLabel.setBounds(0, 0, 773, 549);
		contentPane.add(lblNewLabel);
		table1();
		table2();
	}
	
	public void table1() {
		
		try {
		
				String sql="select Book_ID,Book_Name,Edition,Student_ID,Name From issue";
				Class.forName("com.mysql.jdbc.Driver");
				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
			
				PreparedStatement pst=conn.prepareStatement(sql);
				ResultSet rs=pst.executeQuery();
				table.setModel(DbUtils.resultSetToTableModel(rs));
				
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
		
		
		
	}
		public void table2() {
				
				try {
				
						String sql="select Student_ID,Name,Contact_Number,Book_ID,BName From returnbooks";
						Class.forName("com.mysql.jdbc.Driver");
						Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
					
						PreparedStatement pst=conn.prepareStatement(sql);
						ResultSet rs=pst.executeQuery();
						table_1.setModel(DbUtils.resultSetToTableModel(rs));
						
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}
				
				
				
			}
}
