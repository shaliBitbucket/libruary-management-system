package library;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.EtchedBorder;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.proteanit.sql.DbUtils;
import javax.swing.JOptionPane;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.SystemColor;

public class UserHome extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private JLabel count;
	DefaultTableModel model;
	private String User_Id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserHome frame = new UserHome();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public UserHome() {
		setTitle("User Home");
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Exit");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mntmNewMenuItem.setIcon(new ImageIcon(Home.class.getResource("/image/exit.png")));
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Logout");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				log_in ob =new log_in();
				ob.setVisible(true);
			}
		});
		mntmNewMenuItem_1.setIcon(new ImageIcon(Home.class.getResource("/image/logout.jpg")));
		mnNewMenu.add(mntmNewMenuItem_1);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 828, 472);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 255, 127));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(154, 205, 50));
		panel.setBounds(10, 35, 227, 168);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("I Have ");
		lblNewLabel.setBounds(66, 11, 92, 55);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 26));
		
		JLabel count = new JLabel("2");
		count.setFont(new Font("Tahoma", Font.PLAIN, 60));
		count.setBounds(86, 42, 48, 80);
		panel.add(count);
		
		JLabel lblBooks = new JLabel("Books ");
		lblBooks.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblBooks.setBounds(66, 102, 92, 55);
		panel.add(lblBooks);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.inactiveCaptionBorder);
		panel_1.setBounds(10, 238, 227, 168);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblMyBooks = new JLabel("My Books");
		
		lblMyBooks.setHorizontalAlignment(SwingConstants.CENTER);
		lblMyBooks.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblMyBooks.setBounds(10, 11, 207, 146);
		panel_1.add(lblMyBooks);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Search Book", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(310, 29, 433, 65);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		
		
		textField = new JTextField();
		
		
		
		
		textField.setBounds(10, 21, 402, 26);
		panel_2.add(textField);
		textField.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(271, 105, 520, 301);
		contentPane.add(scrollPane);
		
		table = new JTable();
		model=new DefaultTableModel();
		Object[] columns= {"Book_ID","Book_Name","Edition","Publisher","Price","Pages"};
		Object[] row=new Object[0];
		model.setColumnIdentifiers(columns);
		table.setModel(model);
		scrollPane.setViewportView(table);
		
		//BookCount();
		tableload();
		
		
	}
	
	public void tableload() {
	     
        try {
        	 Class.forName("com.mysql.jdbc.Driver");
         	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
         	
            String sql="SELECT *FROM book";
            PreparedStatement pst=conn.prepareStatement(sql);
            
            ResultSet rs=pst.executeQuery();
            table.setModel(DbUtils.resultSetToTableModel(rs));
            
           


   
          
           
          
           
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            
        }
    
    
    }
	
	

	/**
	 * Create the frame.
	 */
	public UserHome(String user_id) {
		
		User_Id=user_id;
		
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Exit");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mntmNewMenuItem.setIcon(new ImageIcon(Home.class.getResource("/image/exit.png")));
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Logout");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				log_in ob =new log_in();
				ob.setVisible(true);
			}
		});
		mntmNewMenuItem_1.setIcon(new ImageIcon(Home.class.getResource("/image/logout.jpg")));
		mnNewMenu.add(mntmNewMenuItem_1);
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 828, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.PINK);
		panel.setBounds(10, 35, 227, 168);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("I Have ");
		lblNewLabel.setBounds(66, 11, 92, 55);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 26));
		
		JLabel lblNewLabel_1 = new JLabel("2");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 60));
		lblNewLabel_1.setBounds(86, 42, 48, 80);
		panel.add(lblNewLabel_1);
		
		JLabel lblBooks = new JLabel("Books ");
		lblBooks.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblBooks.setBounds(66, 102, 92, 55);
		panel.add(lblBooks);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.PINK);
		panel_1.setBounds(10, 238, 227, 168);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblMyBooks = new JLabel("My Books");
		lblMyBooks.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				setVisible(false);
				Mybooks ob =new Mybooks(User_Id);
				ob.setVisible(true);
				
			}
		});
		lblMyBooks.setHorizontalAlignment(SwingConstants.CENTER);
		lblMyBooks.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblMyBooks.setBounds(10, 11, 207, 146);
		panel_1.add(lblMyBooks);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Search Book", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(310, 11, 433, 65);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				
				search();
				
			}

			private void search() {
				 String serch=textField.getText();
			        try {
			        	Class.forName("com.mysql.jdbc.Driver");
			        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
			        	
			        		String sql="SELECT *FROM book WHERE Name LIKE '%"+serch+"%' OR Book_ID LIKE '%"+serch+"%'";
			 			   
					           PreparedStatement pst=conn.prepareStatement(sql);
					       
					           
					           ResultSet rs=pst.executeQuery();
					           table.setModel(DbUtils.resultSetToTableModel(rs));
			        		
			        	
			           
			           
			        } catch (Exception e) {
			            JOptionPane.showMessageDialog(null, e);
			        }
			    
			      
			       
			       
				
			}
		});
		
		
		textField.setBounds(10, 21, 306, 26);
		panel_2.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Search");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(334, 23, 89, 23);
		panel_2.add(btnNewButton);
		
		JMenu mnNewMenu1 = new JMenu("File");
		mnNewMenu1.setBackground(Color.GREEN);
		mnNewMenu1.setBounds(0, -2, 115, 26);
		contentPane.add(mnNewMenu1);
		
		JMenuItem mntmNewMenuItem1 = new JMenuItem("Logout");
		mntmNewMenuItem1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false);
				log_in ob =new log_in();
				ob.setVisible(true);
			}
		});
		mnNewMenu1.add(mntmNewMenuItem1);
		
		JMenuItem mntmNewMenuItem_11 = new JMenuItem("Exit");
		mntmNewMenuItem_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
		mnNewMenu1.add(mntmNewMenuItem_11);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(271, 105, 520, 301);
		contentPane.add(scrollPane);
		
		table = new JTable();
		model=new DefaultTableModel();
		Object[] columns= {"Book_ID","Book_Name","Edition","Publisher","Price","Pages"};
		Object[] row=new Object[0];
		model.setColumnIdentifiers(columns);
		table.setModel(model);
		scrollPane.setViewportView(table);
		 
		//BookCount();
		tableload();
	}

	private void BookCount() {
		
		try {
        	Class.forName("com.mysql.jdbc.Driver");
        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/libruarymanagement","root","");
        	
        		String sql="SELECT COUNT(*) FROM issue WHERE Student_ID=='"+User_Id+"'";
        				
 			   
		           PreparedStatement pst=conn.prepareStatement(sql);
		       
		           
		           ResultSet rs=pst.executeQuery();
		           count.setText(sql);


        		
        	
           
           
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    
		
		
	}
}
